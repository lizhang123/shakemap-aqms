Contributing to shakemap-aqms
=============================

Feel free to contribute. We use Black formatting.

Reporting issues
----------------

Use the "Issues" feature of gitlab to report issues.
